///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Daniel Luong<dluong@hawaii.edu>
// @date   15 January 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    int sum=0;
    int i;
   int n = atoi(argv[1]);
      for(i=1;i<n;i++)
      {

   sum=sum+i;

      }
      printf("Value of Sum: %d\n",sum);
   return 0;
}
